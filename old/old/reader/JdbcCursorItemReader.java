package dev.luoma.batch.testing.old.reader;

import dev.luoma.batch.testing.datasource.JdbcDataSourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;

import javax.sql.DataSource;

public class JdbcCursorItemReader<T> extends org.springframework.batch.item.database.JdbcCursorItemReader implements ItemReader {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(JdbcCursorItemReader.class);

    private DataSource dataSource;

    public JdbcCursorItemReader(final String dsAlias) {
        this.dataSource = JdbcDataSourceFactory.getDataSource(dsAlias);
    }

    @Override
    public void setDataSource(DataSource dataSource) {
        throw new UnsupportedOperationException();
    }
}
