package dev.luoma.batch.testing.old.reader;

import dev.luoma.batch.testing.datasource.JdbcDataSourceFactory;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;

public class JdbcItemReaderFactory<T> {
    private DataSource dataSource;
    private String sql;
    private RowMapper<T> rowMapper;
    private String name;
    private boolean saveState = true;
    private int maxItemCount = Integer.MAX_VALUE;

    public JdbcItemReaderFactory<T> datasource(String dsAlias) {
        this.dataSource = JdbcDataSourceFactory.getDataSource(dsAlias);

        return this;
    }

    public JdbcItemReaderFactory<T> sql(String sql) {
        this.sql = sql;

        return this;
    }

    public JdbcItemReaderFactory<T> rowMapper(RowMapper<T> rowMapper) {
        this.rowMapper = rowMapper;

        return this;
    }

    public JdbcItemReaderFactory<T> name(String name) {
        this.name = name;

        return this;
    }

    public JdbcItemReaderFactory<T> saveState(boolean saveState) {
        this.saveState = saveState;

        return this;
    }

    public JdbcItemReaderFactory<T> maxItemCount(int maxItemCount) {
        this.maxItemCount = maxItemCount;

        return this;
    }

    public JdbcCursorItemReader<T> build() {
        JdbcCursorItemReader<T> reader = new JdbcCursorItemReader<>();

        reader.setDataSource(this.dataSource);
        reader.setRowMapper(this.rowMapper);
        reader.setSql(this.sql);
        reader.setName(this.name);
        reader.setMaxItemCount(this.maxItemCount);
        reader.setSaveState(this.saveState);

        return reader;
    }
}
