package dev.luoma.batch.testing.old.reader.writer;

import dev.luoma.batch.testing.datasource.JdbcDataSourceFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import javax.sql.DataSource;

public class JdbcItemWriter<T> extends JdbcBatchItemWriter implements ItemWriter {

    private DataSource dataSource;

    public JdbcItemWriter(final String dsAlias) {
        this.dataSource = JdbcDataSourceFactory.getDataSource(dsAlias);
    }

    @Override
    public void setDataSource(DataSource dataSource) {
        throw new UnsupportedOperationException();
    }
}
