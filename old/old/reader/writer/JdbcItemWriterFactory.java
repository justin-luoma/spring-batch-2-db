package dev.luoma.batch.testing.old.reader.writer;

import dev.luoma.batch.testing.datasource.JdbcDataSourceFactory;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.support.ColumnMapItemPreparedStatementSetter;

import javax.sql.DataSource;
import java.math.BigInteger;
import java.util.Map;

public class JdbcItemWriterFactory<T> extends JdbcBatchItemWriterBuilder {
    private DataSource dataSource;
    private String sql;
    private BigInteger mapped = new BigInteger("0");

    public JdbcItemWriterFactory<T> datasource(String dsAlias) {
        this.dataSource = JdbcDataSourceFactory.getDataSource(dsAlias);
        return this;
    }

    public JdbcItemWriterFactory<T> sql(String sql) {
        this.sql = sql;
        return this;
    }

    public JdbcItemWriterFactory<T> beanMapped() {
        this.mapped = this.mapped.setBit(1);

        return this;
    }

    public JdbcBatchItemWriter<T> build() {
        JdbcBatchItemWriter<T> writer = new JdbcBatchItemWriter<>();
        writer.setDataSource(this.dataSource);
        writer.setSql(this.sql);
        if (this.mapped.intValue() == 1) {
            ((JdbcBatchItemWriter<Map<String,Object>>)writer).setItemPreparedStatementSetter(new ColumnMapItemPreparedStatementSetter());
        }

        return writer;
    }
}
