package dev.luoma.batch.testing.old.listener;

import dev.luoma.batch.testing.datasource.JdbcDataSourceFactory;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

import java.sql.SQLException;

public class JdbcJobListener extends JobExecutionListenerSupport {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(JdbcJobListener.class);

    private String[] dsAliases;

    public JdbcJobListener(String[] dsAliases) {
        this.dsAliases = dsAliases;
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        for (String dsAlias : dsAliases) {
            BasicDataSource ds =
                    (BasicDataSource) JdbcDataSourceFactory.removeDataSource(dsAlias);
            try {
                if (ds != null) {
                    ds.close();
                }
            } catch (SQLException e) {
                LOGGER.warn("Unable to close properly datasource: " + dsAlias, e);
            }
        }
    }
}
