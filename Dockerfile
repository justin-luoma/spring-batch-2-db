FROM openjdk:8-jdk-alpine

VOLUME /tmp

ARG JAR_FILE

ARG RESOURCES

COPY ${JAR_FILE} app.jar

COPY ${RESOURCES} /

EXPOSE 8080

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
