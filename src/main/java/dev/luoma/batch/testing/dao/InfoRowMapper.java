package dev.luoma.batch.testing.dao;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InfoRowMapper implements RowMapper<Info> {
    @Override
    public Info mapRow(ResultSet rs, int rowNum) throws SQLException {
        Info info = new Info();

        info.setID(rs.getLong("id"));
        info.setFirstName(rs.getString("first_name"));
        info.setLastName(rs.getString("last_name"));
        info.setEmail(rs.getString("email"));
        info.setGender(rs.getString("gender"));
        info.setIpAddress(rs.getString("ip_address"));

        return info;
    }
}
