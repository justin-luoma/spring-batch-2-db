package dev.luoma.batch.testing.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "first_name",
        "last_name",
        "email",
        "gender",
        "ip_address"
})

@Data
@NoArgsConstructor
public class Info {

    @JsonProperty("id")
    public Long ID;
    @JsonProperty("first_name")
    public String FirstName;
    @JsonProperty("last_name")
    public String LastName;
    @JsonProperty("email")
    public String Email;
    @JsonProperty("gender")
    public String Gender;
    @JsonProperty("ip_address")
    public String IpAddress;

    public Info(Long ID, String firstName, String lastName, String email,
                String gender, String ipAddress) {
        this.ID = ID;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        Gender = gender;
        IpAddress = ipAddress;
    }

    public Info(Info other) {
        this.ID = other.ID;
        this.FirstName = other.FirstName;
        this.LastName = other.LastName;
        this.Email = other.Email;
        this.Gender = other.Gender;
        this.IpAddress = other.IpAddress;
    }
}
