package dev.luoma.batch.testing.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.*;
import javax.naming.spi.InitialContextFactory;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BatchInitialContextFactory implements InitialContextFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchInitialContextFactory.class);
    private static final Context BATCH_CTX = new BatchContext();

    @Override
    public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
        return BATCH_CTX;
    }

    private static class BatchContext implements Context {
        private final Map<String, Object> data = new ConcurrentHashMap<>();

        @Override
        public void bind(String name, Object obj) {
            data.put(name, obj);
        }

        @Override
        public Object lookup(String name) throws NamingException {
            if (data.containsKey(name)) {
                return data.get(name);
            } else {
                throw new NamingException("Context doesn't contain a value " +
                        "for " + name);
            }
        }

        @Override
        public void unbind(String name) throws NamingException {
            data.remove(name.toString());
        }

        @Override
        public void bind(Name name, Object obj) throws NamingException {
            bind(name.toString(), obj);
        }


        @Override
        public Object lookup(Name name) throws NamingException {
            return lookup(name.toString());
        }


        @Override
        public void unbind(Name name) throws NamingException {
            unbind(name.toString());
        }


        @Override
        public void rebind(String name, Object obj) throws NamingException {
            bind(name, obj);
        }

        @Override
        public void rebind(Name name, Object obj) throws NamingException {
            rebind(name.toString(), obj);
        }

        @Override
        public void close() throws NamingException {
            // Nothing to do here
        }

        // Unsupported
        @Override
        public void rename(Name oldName, Name newName) throws NamingException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void rename(String oldName, String newName) throws NamingException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void destroySubcontext(Name name) throws NamingException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void destroySubcontext(String name) throws NamingException {
            throw new UnsupportedOperationException();
        }

        @Override
        public Context createSubcontext(Name name) throws NamingException {
            return null;
        }

        @Override
        public NamingEnumeration<NameClassPair> list(Name name) throws NamingException {
            return null;
        }

        @Override
        public NamingEnumeration<NameClassPair> list(String name) throws NamingException {
            return null;
        }

        @Override
        public NamingEnumeration<Binding> listBindings(Name name) throws NamingException {
            return null;
        }

        @Override
        public NamingEnumeration<Binding> listBindings(String name) throws NamingException {
            return null;
        }

        @Override
        public Context createSubcontext(String name) throws NamingException {
            return null;
        }

        @Override
        public Object lookupLink(Name name) throws NamingException {
            return null;
        }

        @Override
        public Object lookupLink(String name) throws NamingException {
            return null;
        }

        @Override
        public NameParser getNameParser(Name name) throws NamingException {
            return null;
        }

        @Override
        public NameParser getNameParser(String name) throws NamingException {
            return null;
        }

        @Override
        public Name composeName(Name name, Name prefix) throws NamingException {
            return null;
        }

        @Override
        public String composeName(String name, String prefix) throws NamingException {
            return null;
        }

        @Override
        public Object addToEnvironment(String propName, Object propVal) throws NamingException {
            return null;
        }

        @Override
        public Object removeFromEnvironment(String propName) throws NamingException {
            return null;
        }

        @Override
        public Hashtable<?, ?> getEnvironment() throws NamingException {
            return null;
        }

        @Override
        public String getNameInNamespace() throws NamingException {
            return null;
        }
    }
}
