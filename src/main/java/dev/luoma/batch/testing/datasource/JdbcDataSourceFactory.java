package dev.luoma.batch.testing.datasource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.NoInitialContextException;
import javax.sql.DataSource;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

public class JdbcDataSourceFactory {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(JdbcDataSourceFactory.class);
    private static final Map<String, DataSource> dsPool = new HashMap<>();
    private static final String JNDI_STRING = "java:comp/env/jdbc/";

    private String dsAlias;

    private String url;
    private String testUrl;

    private String className;

    private String username;
    private String password;

    public JdbcDataSourceFactory alias(String dsAlias) {
        this.dsAlias = dsAlias;
        return this;
    }

    public JdbcDataSourceFactory url(String url) {
        this.url = url;

        return this;
    }

    public JdbcDataSourceFactory testUrl(String testUrl) {
        this.testUrl = testUrl;

        return this;
    }

    public JdbcDataSourceFactory className(String className) {
        this.className = className;

        return this;
    }

    public JdbcDataSourceFactory username(String username) {
        this.username = username;

        return this;
    }

    public JdbcDataSourceFactory password(String password) {
        this.password = password;

        return this;
    }

    public DataSource build() {
        BasicDataSource ds = new BasicDataSource();
        ds.setUrl(url);
        String env = System.getenv("ENVIRONMENT");
        if ((env != null && (env.equalsIgnoreCase("DEV") || env.equalsIgnoreCase("TEST")) && testUrl != null)) {
            LOGGER.info(String.format("ENVIRONMENT: %s, using testUrl: %s", env, testUrl));
            ds.setUrl(testUrl);
        }
//        if ((username == null) && dsAlias != null) {
//            username = System.getenv(dsAlias + "_ID");
//            password = System.getenv(dsAlias + "_PW");
//        } else {
//            throw new InvalidParameterException();
//        }
        ds.setDefaultAutoCommit(false);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setDriverClassName(className);

        JdbcDataSourceFactory.registerDataSource(dsAlias, ds);

        return JdbcDataSourceFactory.getDataSource(this.dsAlias);
    }

    private static void registerDataSource(String dsAlias, DataSource ds) {
        synchronized (JdbcDataSourceFactory.class) {
            dsPool.putIfAbsent(dsAlias, ds);
            bindJNDI(dsAlias, ds, false);
        }
    }

    private static void bindJNDI(String dsAlias, DataSource ds,
                                 boolean isRetry) {
        try {
            Context context = new InitialContext();
            try {
                context.bind(JNDI_STRING + dsAlias, ds);
            } finally {
                context.close();
            }
        } catch (NoInitialContextException e) {
            if (isRetry) {
                LOGGER.warn("Unable to initialize JNDI InitialContext");
            } else {
                System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                        BatchInitialContextFactory.class.getName());
                bindJNDI(dsAlias, ds, true);
            }
        } catch (NamingException e) {
            LOGGER.warn("Unable to bind data source: " + dsAlias + " into " +
                    "JNDI", e);
        }
    }

    public static DataSource removeDataSource(String dsAlias) {
        synchronized (JdbcDataSourceFactory.class) {
            return dsPool.remove(dsAlias);
        }
    }

    public static DataSource getDataSource(String dsAlias) {
        DataSource ds = null;
        try {
            ds = (DataSource) new InitialContext().lookup(JNDI_STRING + dsAlias);
        } catch (NamingException e) {
            LOGGER.warn("Unable to lookup data source: " + dsAlias + " in " +
                    "JNDI", e);
        }
        if (ds == null) {
            ds = dsPool.get(dsAlias);
        }
        return ds;
    }
}
