package dev.luoma.batch.testing.business;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;

import java.util.Collection;

@AllArgsConstructor
@Data
public class JobDO {
    private JobInstance jobInstance;
    private ExitStatus exitStatus;
    private Collection<Throwable> exceptions;
}
