package dev.luoma.batch.testing.config;



import dev.luoma.batch.testing.datasource.JdbcDataSourceFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DatasourceConfig {

    @Bean(name = "meta")
    @Primary
    public DataSource configureDataSource(@Value("${ds0-alias}") String dsAlias,
                                          @Value("${ds0-url}") String url,
                                          @Value("${ds0-test-url}") String testUrl,
                                          @Value("${ds0-driver-class-name}") String className,
                                          @Value("${ds0-username}") String username,
                                          @Value("${ds0-password}") String password) {
        return new JdbcDataSourceFactory()
                .alias(dsAlias)
                .url(url)
                .testUrl(testUrl)
                .className(className)
                .username(username)
                .password(password)
                .build();
    }

    @Bean(name = "source")
    @StepScope
    public DataSource configureDataSource2(@Value("${ds1-alias}") String dsAlias,
                                           @Value("${ds1-url}") String url,
                                           @Value("${ds1-test-url}") String testUrl,
                                           @Value("${ds1-driver-class-name}") String className,
                                           @Value("${ds1-username}") String username,
                                           @Value("${ds1-password}") String password) {
        return new JdbcDataSourceFactory()
                .alias(dsAlias)
                .url(url)
                .testUrl(testUrl)
                .className(className)
                .username(username)
                .password(password)
                .build();
    }
}
