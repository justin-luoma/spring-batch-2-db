package dev.luoma.batch.testing.config;

import dev.luoma.batch.testing.dao.Info;
import dev.luoma.batch.testing.dao.InfoRowMapper;
import dev.luoma.batch.testing.processor.InfoItemProcessor;
import dev.luoma.batch.testing.writer.JdbcBatchItemWriter;
import dev.luoma.batch.testing.writer.JdbcBatchItemWriterBuilder;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.partition.PartitionHandler;
import org.springframework.batch.core.partition.support.TaskExecutorPartitionHandler;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.sql.DataSource;

@Configuration
public class BatchConfig {

    private JobBuilderFactory jobBuilderFactory;
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    public BatchConfig(JobBuilderFactory jobBuilderFactory,
                       StepBuilderFactory stepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    @StepScope
    public JdbcCursorItemReader<Info> reader(@Qualifier("source") DataSource dataSource) {
        return new JdbcCursorItemReaderBuilder<Info>()
                .name("postgresItemReader")
                .dataSource(dataSource)
                .sql("SELECT * FROM public.mock_data")
                .rowMapper(new InfoRowMapper())
                .build();
    }

    @Bean
    public InfoItemProcessor processor() {
        return new InfoItemProcessor();
    }

    @Bean
    @StepScope
    public JdbcBatchItemWriter<Info> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Info>()
                .sql("INSERT INTO public.mock_data (id, first_name, " +
                        "last_name, " +
                        "email, gender, ip_address) VALUES (:ID, :FirstName, " +
                        ":LastName, :Gender, :Email, :IpAddress)")
                .dataSource(dataSource)
                .beanMapped()
                .build();
    }

    @Bean
    public Job copyInfoJob(Step step1) {
        return jobBuilderFactory.get("copyInfoJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(4);
        taskExecutor.afterPropertiesSet();
        return taskExecutor;
    }

    @Bean
    public PartitionHandler partitionHandler() {
        TaskExecutorPartitionHandler handler = new TaskExecutorPartitionHandler();
        handler.setTaskExecutor(taskExecutor());
        handler.setGridSize(10);
        return handler;
    }

    @Bean
    public Step step1(JdbcCursorItemReader<Info> reader,
                      JdbcBatchItemWriter<Info> writer) {
        return stepBuilderFactory.get("step1")
                .<Info, Info>chunk(100)
                .reader(reader)
                .processor(processor())
                .writer(writer)
                .build();
    }
}
