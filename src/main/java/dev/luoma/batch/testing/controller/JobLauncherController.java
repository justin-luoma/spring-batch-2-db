package dev.luoma.batch.testing.controller;

import dev.luoma.batch.testing.business.JobDO;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobLauncherController {

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    JobOperator jobOperator;

    @Autowired
    Job job;

    @RequestMapping("/")
    public JobDO root() throws Exception {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
        JobExecution jobExecution = jobLauncher.run(job, jobParameters);
        while (jobExecution.isRunning()) {
            wait(1000);
        }

        return new JobDO(jobExecution.getJobInstance(), jobExecution.getExitStatus(), jobExecution.getAllFailureExceptions());
    }
}
