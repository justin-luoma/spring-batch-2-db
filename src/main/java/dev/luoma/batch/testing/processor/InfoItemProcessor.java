package dev.luoma.batch.testing.processor;

import dev.luoma.batch.testing.dao.Info;
import org.springframework.batch.item.ItemProcessor;

public class InfoItemProcessor implements ItemProcessor<Info, Info> {
    @Override
    public Info process(final Info item) throws Exception {
        final String fn = item.getFirstName();
        final String ln = item.getLastName();

        final Info newInfo = new Info(item);
        newInfo.setFirstName(reverse(fn.toUpperCase()));
        newInfo.setLastName(reverse(ln.toUpperCase()));
        newInfo.setEmail(reverse(item.getEmail()));
        newInfo.setGender(reverse(item.getGender()));
        newInfo.setIpAddress(reverse(item.getIpAddress()));
        return newInfo;
    }

    private static String reverse(String str) {
        StringBuilder sb = new StringBuilder(str);
        return sb.reverse().toString();
    }
}
