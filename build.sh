#!/usr/bin/env bash

./gradlew clean assemble jar

docker container rm spring

docker build \
--build-arg JAR_FILE=build/libs/testing.jar --build-arg RESOURCES=build/resources \
-t spring-boot . \
&& docker run \
-p 8080:8080 \
--name spring \
-h spring \
--network spring-batch \
spring-boot
